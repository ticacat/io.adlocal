var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

io.on('connection', function(socket) {
  console.log(io.sockets)  
  socket.on('captured', function(data) {
    console.log('captured' + data.code)
    io.sockets.emit('captured', data);

    
    
});

  socket.on('new-action', function(data) {
      console.log('new-action: ' + data.action + ' -> ' + data.code)
      
      io.sockets.emit('messages', data);
  });
});

server.listen(80, function() {
  console.log("Servidor corriendo en http://localhost:80");
});
