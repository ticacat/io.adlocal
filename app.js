var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

io.on('connection', function(socket) {

  socket.on('captured', function(data) {
    io.sockets.emit('captured', data);
  });

  socket.on('new-action', function(data) {      
      io.sockets.emit('messages', data);
  });
});

server.listen(3000);
